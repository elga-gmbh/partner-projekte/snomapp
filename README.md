# SNOMAPP

Das 2021 fertiggestellte Studentenprojekt SNOMAPP, welches ein Codemapping zwischen APPC und SNOMED CT ermöglicht. Mappings können als FHIR concept maps exportiert werden. Code unter https://github.com/FHOOEAIST/SNOMAPP auffindbar.